package client;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;


public class MainFrame extends JFrame {
	private JPanel contentPane;

	Client client;
	OrderPane panel;
	String sign;
	String oppSign;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) throws IOException {
//		MainFrame frame = new MainFrame();
//		frame.setVisible(true);

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() throws IOException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 550);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblHost = new JLabel("Host:");
		lblHost.setBounds(10, 14, 40, 14);
		contentPane.add(lblHost);
		loadOrderPane();
		JFormattedTextField frmtdtxtfldIp;
		frmtdtxtfldIp = new JFormattedTextField();
		frmtdtxtfldIp.setBounds(43, 11, 90, 20);
		frmtdtxtfldIp.setText("127.0.0.1");
		contentPane.add(frmtdtxtfldIp);

		JButton btnConnect = new JButton("Connect");
		btnConnect.setBounds(10, 70, 90, 23);
		contentPane.add(btnConnect);

		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					client = new Client("127.0.0.1", 6666);
					play();
				} catch (IOException e1) {
					e1.printStackTrace();
				}

			}
		});


		JFormattedTextField frmtdtxtfldXxxx = new JFormattedTextField();
		frmtdtxtfldXxxx.setText("8080");
		frmtdtxtfldXxxx.setBounds(43, 39, 90, 20);
		contentPane.add(frmtdtxtfldXxxx);

		JLabel lblPort = new JLabel("Port:");
		lblPort.setBounds(10, 42, 40, 14);
		contentPane.add(lblPort);

		JLabel lblNotConnected = new JLabel("Not Connected");
		lblNotConnected.setForeground(new Color(255, 255, 255));
		lblNotConnected.setBackground(new Color(128, 128, 128));
		lblNotConnected.setOpaque(true);
		lblNotConnected.setBounds(10, 104, 123, 23);
		contentPane.add(lblNotConnected);

	}

	void loadOrderPane() throws IOException {
		panel = new OrderPane( this);
		panel.setBounds(145, 14, 487, 448);
		contentPane.add(panel);
		contentPane.revalidate();
		contentPane.repaint();
	}


	public void play() throws IOException {
		String serverResponse;
		try {
			 serverResponse = client.in.readLine();
			if (serverResponse.startsWith("WELCOME")) {
				setSigns(serverResponse);
//				return;


			}
			while (true) {
				serverResponse = client.in.readLine();
//				if(serverResponse!=null)
//				{

					if (serverResponse.startsWith("VALID_MOVE")) {
						JOptionPane.showMessageDialog(this, "Valid move, please wait!");
						panel.setButton(panel.currentField, sign);
						panel.currentField.repaint();
						break;
					} else if (serverResponse.startsWith("OPPONENT_MOVED")) {
						int loc = Integer.parseInt(serverResponse.substring(15));
						panel.setButton(panel.board[loc], oppSign);
						panel.board[loc].repaint();
						JOptionPane.showMessageDialog(this, "Opponent moved, your turn!");
						break;
					} else if (serverResponse.startsWith("VICTORY")) {
						JOptionPane.showMessageDialog(this, "You win!");
						break;
					} else if (serverResponse.startsWith("DEFEAT")) {
						JOptionPane.showMessageDialog(this, "You lose!");
						break;
					} else if (serverResponse.startsWith("TIE")) {
						JOptionPane.showMessageDialog(this, "You tied!");
						break;
					} else if (serverResponse.startsWith("MESSAGE")) {
						JOptionPane.showMessageDialog(this, serverResponse.substring(8));
						break;
					}
				}

//			}

		} finally {
//			client.out.println("QUIT");
			client.socket.close();
		}


	}

	void setSigns(String serverResponse) {
		if (serverResponse.startsWith("WELCOME")) {
			sign = ""+ serverResponse.charAt(8);
		}
		if (sign.equals("o")) {
			oppSign = "x";
		} else oppSign = "o";
	}

	class OrderPane extends JPanel implements ActionListener {

		JButton currentField;
		KKButton[] board = new KKButton[9];
		/**
		 * Create the panel.
		 */
		public OrderPane(MainFrame mainFrame) {
			setBackground(new Color(255, 255, 240));

			setLayout(new GridLayout(3, 3, 5, 5));

			for(int i =0; i<9; i++)
			{
				int nr = i;
				JButton btn = new KKButton();
				board[i]= (KKButton) btn;
				btn.setMnemonic(i);
				btn.addActionListener(this);
				add(btn);
//				btn.addActionListener(new ActionListener() {
//					@Override
//					public void actionPerformed(ActionEvent e) {
//
//							currentField = btn;
//							sendMove("MOVE " + nr);
////							client.out.println("MOVE " + nr);
//					}
//				});

			}
		}

		String setButton(JButton button, String sign)
		{
			if(sign.equals("o"))
			{
				setOinButton(button);
				return "O";
			}
			else
			{
				setXinButton(button);
				return "X";
			}
		}

		void setXinButton(JButton button)
		{
			button.setText("X");
			button.setFont(new Font("Arial",Font.PLAIN,160));
			button.revalidate();
			button.repaint();
		}

		void setOinButton(JButton button)
		{
			button.setText("O");
			button.setFont(new Font("Arial",Font.PLAIN,160));
			button.revalidate();
			button.repaint();
		}

		void sendMove(String move)
		{
			client.out.println(move);
		}

		public void actionPerformed(ActionEvent e) {
			KKButton button = (KKButton) e.getSource();
			currentField = button;
		int a = button.getMnemonic();
				sendMove("MOVE " + button.getMnemonic());
		}
	}
}