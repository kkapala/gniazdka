package client;

import java.awt.*;
import java.io.*;
import java.net.Socket;

public class Client {

	Socket socket;
	PrintWriter out;
	BufferedReader in;


	public Client(String host, int port) throws IOException {
		socket = new Socket(host,port);
		in = new BufferedReader((new InputStreamReader(socket.getInputStream())));
		out = new PrintWriter(socket.getOutputStream(),true);
	}
}
