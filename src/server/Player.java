package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Player extends Thread
{
    Socket socket;
    BufferedReader in;
    PrintWriter out;

    public Player (Socket socket) throws IOException {
        this.socket = socket;
        in = new BufferedReader((new InputStreamReader(socket.getInputStream())));
        out = new PrintWriter(socket.getOutputStream(),true);
    }

    void startPlayer() throws IOException {
        try {
            String cameFromClient = in.readLine();
            System.out.println(cameFromClient);
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            socket.close();
        }

    }

}
