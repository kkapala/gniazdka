package server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Konsola {

//	static ServerSocket serverSocket;
//	static Socket socket;


	public  static void main(String[] args) throws IOException {
		ServerSocket serverSocket = new ServerSocket(6666);


		try {
			while(true)
			{
				Game game = new Game();
				Game.Player playerX = game.new Player(serverSocket.accept(),"x");
				Game.Player playerO = game.new Player(serverSocket.accept(),"o");
				playerX.setOpponent(playerO);
				playerO.setOpponent(playerX);
				game.movingPlayer = playerX;
				playerX.start();
				playerO.start();
			}
		}
		finally {
			serverSocket.close();
		}
	}
}
