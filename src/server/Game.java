package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Game
{
    Player movingPlayer;
    List<String> board;

    Game()
    {
         board = new ArrayList<>(Arrays.asList("e", "e", "e", "e", "e", "e", "e", "e", "e"));
    }

    boolean isFinished()
    {
        for (String field:board)
        {
            if(field.equals("e"))
                return false;
        }
        return true;
    }

    boolean isWin()
    {
        return (board.get(0)!="e"&&board.get(0)==board.get(4)&&board.get(0)==board.get(8)
                ||board.get(2)!="e"&&board.get(2)==board.get(4)&&board.get(2)==board.get(6)
                ||board.get(0)!="e"&&board.get(0)==board.get(3)&&board.get(0)==board.get(6)
                ||board.get(1)!="e"&&board.get(1)==board.get(4)&&board.get(1)==board.get(7)
                ||board.get(2)!="e"&&board.get(2)==board.get(5)&&board.get(2)==board.get(8)
                ||board.get(0)!="e"&&board.get(0)==board.get(1)&&board.get(0)==board.get(2)
                ||board.get(3)!="e"&&board.get(3)==board.get(4)&&board.get(3)==board.get(5)
                ||board.get(6)!="e"&&board.get(6)==board.get(7)&&board.get(6)==board.get(8));
    }

    boolean canDoMove(int field, Player player)
    {
        if(player ==movingPlayer && board.get(field).equals("e"))
        {
            board.set(field,player.sign);
            movingPlayer=player.opponent;
            movingPlayer.opponentMoved(field);
            return true;
        }
        else return false;
    }

class Player extends Thread
{
    Socket socket;
    BufferedReader in;
    PrintWriter out;
    String sign;
    Player opponent;

    public Player (Socket socket, String sign) throws IOException {
        this.socket = socket;
        this.sign = sign;
        in = new BufferedReader((new InputStreamReader(socket.getInputStream())));
        out = new PrintWriter(socket.getOutputStream(),true);
        out.println("WELCOME "+sign);
    }

    void setOpponent(Player opponent)
    {
        this.opponent=opponent;
    }

    void opponentMoved(int field)
    {
        out.println("OPPONENT_MOVED "+field);
        out.println(isWin()?"DEFEAT":isFinished()?"TIE":"");
    }

    public void run()
    {

        try{
            if(sign.equals("x"))
                out.println("MESSAGE  Your move");

            while (true)
            {
                out.println("something");
                String clientResponse = in.readLine();
                out.println(clientResponse);
                if(clientResponse!=null) {
                    if (clientResponse.startsWith("MOVE")) {
                        int field = Integer.parseInt(clientResponse.substring(5));
                        if (canDoMove(field, this)) {
                            out.println("VALID_MOVE");
                            out.println(isWin() ? "VICTORY" : isFinished() ? "TIE" : "");
                        } else if (clientResponse.startsWith("QUIT"))
                            return;
                    }
                }
            }

        }
        catch (IOException e)
        {
            System.out.println("To to miejsce "+e);
        }
        finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
}
